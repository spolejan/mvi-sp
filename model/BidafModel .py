import keras
import json
import numpy as np
from keras import backend as K
import tensorflow as tf
from sklearn.metrics.pairwise import cosine_similarity
from typing import List
from keras.models import Model
from keras.layers import Bidirectional, LSTM, Dense, Embedding, add, dot, Input, Activation, Lambda
from keras.utils import to_categorical
#import attention_layer

def model():
    context_train = json.load(open('data/squad/context_train.json'))
    print('Context train read')
    query_train = json.load(open('data/squad/query_train.json'))
    print('Query train read')
    shared = json.load(open('data/squad/shared.json'))
    print('shared  read')

    word_emb_size = 100
    num_classes = T = len(context_train['context_idx'][0])
    print(T)
    J = len(query_train['question_idx'][0])

    y_train_start, y_train_end = [], [] 
    for x, y in zip(query_train['answer_start'], query_train['answer_end']):
        y_train_start.append(x[0])
        y_train_end.append(y[0])

    y_train_start_encoded = to_categorical(y_train_start, num_classes)
    y_train_end_encoded = to_categorical(y_train_end, num_classes)

    emb_mat = np.array([shared['word2vec'][word] if word in shared['word2vec']
                            else np.random.multivariate_normal(np.zeros(word_emb_size), np.eye(word_emb_size))
                            for word in shared['all_words']])

    print(emb_mat.shape)
    model1_input = Input(shape = (None,866))
    model1_embed = Embedding(output_dim=100, input_dim=emb_mat.shape[0], weights = [emb_mat], trainable = False)(model1_input)
    print(model1_embed)
    model1_lstm = Bidirectional(LSTM(256, return_sequences = True))(model1_embed)

    model2_input = Input(shape = (J, ))
    model2_embed = Embedding(output_dim=100, input_dim=emb_mat.shape[0], weights = [emb_mat], trainable = False)(model2_input)
    model2_lstm = Bidirectional(LSTM(256, return_sequences = True))(model2_embed)

    multiply_layer = Lambda(attention_layer(model1_lstm,model2_lstm))

    modelling_bilstm_layer = Bidirectional(LSTM(256, return_sequences = True))(multiply_layer)
    modelling_bilstm_layer_1 = Bidirectional(LSTM(256))(modelling_bilstm_layer)
    modelling_bilstm_layer_return_sequences = Bidirectional(LSTM(256, return_sequences = True))(modelling_bilstm_layer) 

    output_start_index = Dense(num_classes,activation='softmax')(modelling_bilstm_layer_1)

    output_end_index_lstm = LSTM(num_classes)(modelling_bilstm_layer_return_sequences)
    merge = add([output_end_index_lstm,output_start_index])
    output_end_index_lstm = Activation('softmax')(merge)


    final_model = Model(inputs=[model1_input, model2_input], outputs=[output_start_index, output_end_index_lstm])

    final_model.compile(optimizer='rmsprop', loss='categorical_crossentropy',metrics=['accuracy'])

    final_model.summary()

sess = tf.Session()
combination = '1,2,1*2,1*3'
combinations = combination.split(",")

def attention_layer(matrix_1,matrix_2):
        matrix_1 = matrix_1.eval(session=sess)
        matrix_2 = matrix_2.eval(session=sess)
        matrix_1 = keras.utils.normalize(matrix_1)
        matrix_2 = keras.utils.normalize(matrix_2)
        num_rows_1 = K.shape(matrix_1)[1]   #no_of_words_in_passage
        num_rows_2 = K.shape(matrix_2)[1]   #no_of_words_in_query
        print(num_rows_2)
        tile_dims_1 = K.concatenate([[1, 1], [num_rows_2], [1]], 0)    #concatenate size of h and u for making it as a same size
        tile_dims_2 = K.concatenate([[1], [num_rows_1], [1, 1]], 0)
        tiled_matrix_1 = K.tile(K.expand_dims(matrix_1, axis=2), tile_dims_1)
        tiled_matrix_2 = K.tile(K.expand_dims(matrix_2, axis=1), tile_dims_2)
        #Finds the similarity of h and u
        cos_sim = K.sum(K.l2_normalize(tiled_matrix_1,axis = -1) * K.l2_normalize(tiled_matrix_2,axis = -1),axis =-1)  
        #Context to query  
        atj = K.softmax(cos_sim) #attention vector
        matrix_2 = tf.constant(matrix_2)
        num_attention_dims = K.ndim(atj)      
        num_matrix_dims = K.ndim(matrix_2) - 1
        for _ in range(num_attention_dims - num_matrix_dims):
            matrix_2 = K.expand_dims(matrix_2, axis=1)   #making the dimension of atj and matrix_2 same
        u_aug = K.sum(K.expand_dims(atj, axis=-1) * matrix_2, -2)    #finding U_Aug
        #Query to Context
        question_passage_similarity = K.max(cos_sim, axis=-1)
        btj = K.softmax(question_passage_similarity)  #Attention Vector
        matrix_1 = tf.constant(matrix_1)
        num_attention_dims2 = K.ndim(btj)
        num_matrix_dims2 = K.ndim(matrix_1) - 1
        for _ in range(num_attention_dims2 - num_matrix_dims2):
            matrix_1 = K.expand_dims(matrix_1, axis=1)
        h_aug =  K.sum(K.expand_dims(btj, axis=-1) * matrix_1, -2)
        h_aug_final = find_h_aug(h_aug = h_aug,matrix= matrix_1)   #finding H_Aug
        final_pass = final_passage([matrix_1,u_aug,h_aug_final])   #finding G
        return final_pass
        
        

def find_h_aug(h_aug,matrix):
    to_repeat = h_aug
    to_copy = matrix
    expanded = K.expand_dims(to_repeat, axis=1)
    ones = [1] * K.ndim(expanded)
    num_repetitions = K.shape(to_copy)[1]
    tile_shape = K.concatenate([ones[:1], [num_repetitions], ones[2:]], 0)
    h_aug_final = K.tile(expanded,tile_shape)
    return h_aug_final



def combination(combination: str, tensors: List['Tensor']):
        if combination.isdigit():
            return tensors[int(combination) - 1]  # indices in the combination string are 1-indexed
        else:
            first_tensor = _get_combination(combination[0], tensors)
            second_tensor = _get_combination(combination[2], tensors)
            if K.int_shape(first_tensor) != K.int_shape(second_tensor):
                shapes_message = "Shapes were: {} and {}".format(K.int_shape(first_tensor),K.int_shape(second_tensor))
            operation = combination[1]
            if operation == '*':
                return first_tensor * second_tensor


def final_passage(x):
        combined_tensor = combination(combinations[0], x)
        for combination in combinations[1:]:
            to_concatenate = _get_combination(combination, x)
            combined_tensor = K.concatenate([combined_tensor, to_concatenate], axis=-1)
        return combined_tensor


model()