# Attention-based question answering

Implementujte a natrénujte neuronovou síť "Bi-Directional Attention Flow", která bude na vstupu přijímat krátký text a otázku. Na výstupu bude odpověď na vstupní otázku - podřetězec vstupního textu.
Na trénování použijte nejlépe Stanford Question Answering Dataset, pro embedding lze použít například předtrénované vektory GloVe.


pro stažení dat 
[http://nlp.stanford.edu/data/glove.6B.zip](http://nlp.stanford.edu/data/glove.6B.zip)

ze staženého zipu vzít jen glove.6B.100d.txt a nakopírovat ho do /data/glove

Pro preprocesing 
`python ./preprocessing/preprocessing.py`

Pro trénování 
`python ./model/BidafModel.py`


